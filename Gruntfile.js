'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bower: {
      install: {
        options: {
          targetDir: 'public/components'
        }
      }
    },
    express: {
      test: {
        options: {
          script: 'index.js',
        }
      }
    },
    karma: {
      options: {
        configFile: 'test/karma.conf.js'
      },
      continuous: {
        singleRun: true
      }
    },
    protractor: {
      options: {
        configFile: 'test/protractor-conf.js',
        noColor: false,
        args: {}
      },
      e2e: {
        options: {
          keepAlive: false
        }
      },
      continuous: {
        options: {
          keepAlive: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-protractor-runner');

  grunt.registerTask('default', ['bower', 'express:test', 'karma', 'protractor:e2e']);
};
