#Coupons - Code Challange

### Configuração

Clone o [repotório da aplicação](https://bitbucket.org/rafaeldias_/coupons).

Entre no diretório da aplicação: 

`$ cd coupons`

Use o comando npm para instalar as dependências:

`$ npm install`.

### Testando

Se não tiver um Selenium Server já instalado, use o seguinte comando:

`$ ./node_modules/protractor/bin/webdriver-manager update`

Use o `grunt` no diretório da aplicação para testar:

`$ grunt` 

### Iniciando

No diretório da aplicação, use o seguinte comando para iniciar:

`$ node index.js`

A aplicação vai, por padrão, iniciar no endereço:

`http://localhost:3000`.

Há uma API básica para inserção de cupons e buscas (`POST` e `GET`):

`http://localhost:3000/api/coupons/:id?`.
