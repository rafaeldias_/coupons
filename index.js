'use strict';

var fs = require('fs')
  , stdio = require('./config/stdio.json');

fs.writeFile(stdio.coupon_out, '"ID";"Coupon";"CPF"\n', function(err) {
  if (err) {
    console.log(err);
    return;
  }
  // Carrega cupons disponíveis na memória.
  fs.readFile(stdio.coupon_in, function(err, data) {
    if (err) {
      console.log(err);
      return;
    }

    var coupons = data.toString().split(/\r?\n|\r|^/gi).filter(
          function(val) { 
            return val.length; 
          }) 
      , uid = 0
      , bodyParser = require('body-parser')
      , express = require('express')
      , app = express();

    app.use(express.static(__dirname + '/public'));
    app.use(bodyParser.json());

    app.route('/api/coupons/:id?')
      .get(function(req, res) {
        fs.exists(stdio.coupon_out, function(exists) {
          if (exists) {
            fs.readFile(stdio.coupon_out, function(err, data) {
              if (err) {
                res.status(500).json({ error: err });
                return;
              }

              var lines = data.toString().split(/\r?\n|\r|^/gi), coupons = [];

              for (var i = 1, line, coupon; line = lines[i]; i++) {
                coupon = line.split(',');
                coupons.push({
                  id: coupon[0],
                  coupon: coupon[1],
                  cpf: coupon[2]
                });
              }

              if (req.params.id ) {
                if ('undefined' !== typeof coupons[req.params.id-1]) {
                  res.status(200).json(coupons[req.params.id-1]);
                } else {
                  res.status(404).json({ error: 'Cupom não encontrado' });
                }
              } else {
                res.status(200).json(coupons);
              }
            })
          } else {
            res.status(404).json({ error: 'Cupom não encontrado' });
          }
        });
      })
      .post(function(req, res) {
        var data;

        if (coupons.length) {
          if ('undefined' === typeof req.body.cpf || !/^\d{11}$/.test(req.body.cpf)) {
            res.status(400).json({ error: 'CPF inválido' });
            return;
          }

          data = [++uid, coupons.splice(0, 1), req.body.cpf]
          fs.appendFile(stdio.coupon_out, data.join(',') + '\n', function(err) {
            if (err) {
              res.status(500).json({ error: err });
              return;
            }
              
            res.status(200).json({ 
              id: uid,
              coupon: data[1],
              cpf: req.body.cpf
            });
          })
        } else {
          res.status(500).json({ error: 'Limite de cupons alcançado.' });
        } 
      });

    var server = app.listen(process.env.PORT || 3000, function() {
      var addr = server.address();
      console.log('Aplicação iniciada em http://%s:%s', addr.address, addr.port);
    });
  });
});
