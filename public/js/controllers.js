'use strict';

var couponsApp = angular.module('couponsApp', ['couponServices']);

couponsApp.controller('CouponsCtrl', ['$scope', 'Coupon', function($scope, Coupon) {
  $scope.generateCoupon = function() {
    if (this.cpf && /^\d{11}$/.test(this.cpf)) {
      Coupon.create({cpf: this.cpf}, function(res) {
        $scope.status = 'success';
        $scope.message = 'Cupom criado com sucesso: ' + res.coupon;
      }, function(res) { 
        if (res.data.error) {
          $scope.status = 'danger'
          $scope.message = res.data.error;
        }
      });
    } else {
      $scope.status = 'danger';
      $scope.message = 'CPF inválido';
    }
  };
}]);
