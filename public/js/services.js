'use strict';

var couponServices = angular.module('couponServices', ['ngResource']);

couponServices.factory('Coupon', ['$resource', function($resource) {
  return $resource('api/coupons/:id', {}, { create: { method: 'POST', params: { id: null } }
  });
}]);
