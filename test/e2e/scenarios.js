'use strict';

describe('Coupon App', function() {
  describe('tests', function() { 
    var msg, cpf, btn;

    beforeEach(function() {
      browser.get('index.html');
      msg = element(by.css('.alert'));
      cpf = element(by.model('cpf'));
      btn = element(by.css('button'));
    });

    it('should alert an error for invalid input', function() { 
      expect(msg.isDisplayed()).toBeFalsy();
      cpf.sendKeys('hjsadjk142js');
      btn.click();
      
      browser.wait(protractor.ExpectedConditions.visibilityOf(msg), 3000);

      expect(msg.getAttribute('class')).toMatch('danger'); 
    });

    it('should alert success for valid input', function() { 
      expect(msg.isDisplayed()).toBeFalsy();
      cpf.sendKeys('37915315811');
      btn.click();

      browser.wait(protractor.ExpectedConditions.visibilityOf(msg), 3000);

      expect(msg.getAttribute('class')).toMatch('success'); 
    }); 
  }); 
});
