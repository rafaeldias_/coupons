'use strict';

describe('REST services', function() {
  beforeEach(module('couponsApp'));
  beforeEach(module('couponServices'));

  describe('CouponsCtrl', function() {
    var scope, ctrl, $httpBackend, validCpf = '37915315811';

    beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
      $httpBackend = _$httpBackend_;
      $httpBackend
        .expectPOST('api/coupons', { cpf: validCpf })
          .respond(200, { id: 'xx', coupon: 'xxx', cpf: 'xxxxxxxxxxx'});

      scope = $rootScope.$new();
      ctrl = $controller('CouponsCtrl', { $scope: scope });
    }));

    it('creates new Coupon', function() {
      scope.cpf = validCpf;
      scope.generateCoupon();
      $httpBackend.flush();

      expect(scope.status).toBe('success');
    });
  });
});
